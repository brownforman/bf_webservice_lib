<?php

/***********************************************************
// BF WebServices Connection Class
// Created By	: Matt Johnson, Lead Interactive Developer
// Production	: Coolfire Media
// Agency		: Boxing-Clever
// Platform		: CodeIgniter
// Date			: November 22, 2011
***********************************************************/
namespace BF\WebServices;

class BFWebServices {

	var $endpoint = "https://proof.b-fonline.com/GeneralWebServices/GenericWebServices/bfonlinewebservices.asmx?WSDL";
	var $live_endpoint = "https://www.b-fonline.com/GeneralWebServices/GenericWebServices/bfonlinewebservices.asmx?WSDL";
	var $envelope = "https://schemas.xmlsoap.org/soap/envelope/";

	var $client;
	var $client_options = array();

	public function __construct($mode="auto",$trace=0,$cache_wsdl=0,$exceptions=0)
    {
		// if mode is auto, check BF env variable
		if ($mode=='auto')
		{
			if ( getenv('APPLICATION_ENV') == 'staging' ) { $mode = 'proof'; } else { $mode = 'live'; }
		}

		// if !proof set to prod to prevent possible live entries in proof db
		if ($mode != 'proof') $this->endpoint = $this->live_endpoint;

		$this->client_options["trace"] = $trace;
		$this->client_options["cache_wsdl"] = $cache_wsdl;
		$this->client_options["exceptions"] = $exceptions;
		$this->client = new \SoapClient($this->endpoint, $this->client_options);
    }

	/****************************************************************************************************
	// BF - GetCountries
	****************************************************************************************************/

	public function getCountries()
	{
		$countries = array();

		try{
			$result = $this->client->GetCountryList();
			$xml = $result->GetCountryListResult->any;

			$response = simplexml_load_string($xml, NULL, false, $this->envelope);
			$country_nodes = $response->xpath('//NewDataSet/countries/country');

			foreach($country_nodes as $country) {
				$current = (string) $country;
				$countries[$current] = $current;
			}
		}
		catch(Exception $e){
			$countries = array();
		}

		return $countries;
	}

	/****************************************************************************************************
	// BF - GetCountryOptions - Create Country Dropdown Contents w/ Selection
	****************************************************************************************************/

	public function getCountryOptions($selected="")
	{
		$options = "";
		$countries = $this->getCountries();

		if( is_array($countries) )
		{
			foreach($countries as $key => $value){
				$extra = "";
				if($value == $selected) $extra = ' selected="selected"';
				$options .= '<option value="'.trim($value).'"'.$extra.'>'.trim($key).'</option>'."\n";
			}
			$options = rtrim($options,"\n");
		}

		return $options;
	}

	/****************************************************************************************************
	// BF - GetStates
	****************************************************************************************************/

	public function getStates()
	{
		$states = array();

		try{
			$result = $this->client->GetStateList();
			$xml = $result->GetStateListResult->any;

			$response = simplexml_load_string($xml, NULL, false, $this->envelope);
			$state_names = $response->xpath('//NewDataSet/states/state');
			$state_abbrv = $response->xpath('//NewDataSet/states/state_abbrv');

			for($i=0; $i<count($state_names); $i++){
				$key = (string) $state_names[$i];
				$value = (string) $state_abbrv[$i];
				$states[$key] = $value;
			}
		}
		catch(Exception $e){
			$states = array();
		}

		return $states;
	}

	/****************************************************************************************************
	// BF - GetStateOptions - Create State Dropdown Contents w/ Selection
	****************************************************************************************************/

	public function getStateOptions($selected="")
	{
		$options = "";
		$states = $this->getStates();

		if( is_array($states) )
		{
			foreach($states as $key => $value){
				$extra = "";
				if($value == $selected) $extra = ' selected="selected"';
				$options .= '<option value="'.trim($value).'"'.$extra.'>'.trim($key).'</option>'."\n";
			}
			$options = rtrim($options,"\n");
		}

		return $options;
	}

	/****************************************************************************************************
	// BF - GetTitles
	****************************************************************************************************/

	public function getTitles()
	{
		$titles = array();

		try{
			$result = $this->client->GetTitles();
			$xml = $result->GetTitlesResult->any;

			$response = simplexml_load_string($xml, NULL, false, $this->envelope);
			$title_nodes = $response->xpath('//NewDataSet/titles/title');

			foreach($title_nodes as $title) {
				$titles[] = (string) $title;
			}
		}
		catch(Exception $e){
			$titles = array();
		}

		return $titles;
	}

	/****************************************************************************************************
	// BF - GetSubTitles
	****************************************************************************************************/

	public function getSubTitles()
	{
		$subtitles = array();

		try{
			$result = $this->client->GetSubTitles();
			$xml = $result->GetSubTitlesResult->any;

			$response = simplexml_load_string($xml, NULL, false, $this->envelope);
			$subtitle_nodes = $response->xpath('//NewDataSet/titles/subtitle');

			foreach($subtitle_nodes as $subtitle) {
				$subtitles[] = (string) $subtitle;
			}
		}
		catch(Exception $e){
			$subtitles = array();
		}

		return $subtitles;
	}

	/****************************************************************************************************
	// BF - ValidateLDA
	****************************************************************************************************/

	public function validateLDA($mm,$dd,$yy,$country,$brand)
	{
		$result = "false";

		try{
			$params = array("strMonth"=>$mm,"strDay"=>$dd,"strYear"=>$yy,"strCountry"=>$country,"iBrandId"=>$brand);
			$validation = $this->client->ValidateLDA($params);
			$result = $validation->ValidateLdaResult;
			$result === TRUE ? $result = "true" : $result = "false";
		}
		catch(Exception $e){
			$result = "false";
		}

		return $result;
	}

	/****************************************************************************************************
	// BF - InsertPromotionResponse
	****************************************************************************************************/
	public function insertPromotionResponse($consumerId, $brandId, $sourceCode, $question, $answer, $promotionURL){
		$result = true;
		try{
			$params = array(
				'iConsumerId' => $consumerId,
				'iBrandId' => $brandId,
				'iEntryId' => $sourceCode,
				'strQuestion' => $question,
				'strAnswer' => $answer,
				'strURL' => $promotionURL
			);
			$result = $this->client->InsertPromotionResponse($params);
		}
		catch(Exception $e){
			$result = false;
		}
		return $result;
	}

	/****************************************************************************************************
	// BF - CreateConsumers - RegisterByString
	****************************************************************************************************/

	//public function createConsumer($fields)
	public function createConsumer($birth_month, $birth_day, $birth_year, $source_code, $segment_id, $brand_id, $first_name1 = '',
								   $mi = '', $last_name1 = '', $address1 = '', $address2 = '', $city = '', $state_prov_terr = '',
								   $zip = '', $gender = '', $country = '', $state_abbrv = '', $email = '', $phone_number = '',
								   $sub_text = '', $title_text = '', $first_name2 = '', $last_name2 = '', $address3 = '',
								   $address4 = '', $address5 = '', $industry_flag = '', $eve_phone = '', $cell_phone = '',
								   $company = '', $fax = '', $guid = '', $send_email = '', $send_mail = '', $send_sms = '',
								   $have_tried = '', $recommend = '', $page_name = '', $url = '', $media_code = '', $call_me = '',
								   $frequency = '', $loyalty = '', $share = '', $method = '', $favorite_way = '', $also_enjoys = '',
								   $last_time = '', $ave_no_drinks = '', $bar_or_rest = '', $last_ten = '', $how_other = '', $lang_id = '')
	{
		$result = 0;

		$fields = array(
			'birth_month' => $birth_month,
			'birth_day' => $birth_day,
			'birth_year' => $birth_year,
			'source_code' => $source_code,
			'segment_id' => $segment_id,
			'brand_id' => $brand_id,
			'first_name1' => $first_name1,
			'mi' => $mi,
			'last_name1' => $last_name1,
			'address1' => $address1,
			'address2' => $address2,
			'city' => $city,
			'state_prov_terr' => $state_prov_terr,
			'zip' => $zip,
			'gender' => $gender,
			'country' => $country,
			'state_abbrv' => $state_abbrv,
			'email' => $email,
			'phone_number' => $phone_number,
			'sub_text' => $sub_text,
			'title_text' => $title_text,
			'first_name2' => $first_name2,
			'last_name2' => $last_name2,
			'address3' => $address3,
			'address4' => $address4,
			'address5' => $address5,
			'industry_flag' => $industry_flag,
			'eve_phone' => $eve_phone,
			'cell_phone' => $cell_phone,
			'company' => $company,
			'fax' => $fax,
			'guid' => $guid,
			'send_email' => $send_email,
			'send_mail' => $send_mail,
			'send_sms' => $send_sms,
			'have_tried' => $have_tried,
			'recommend' => $recommend,
			'page_name' => $page_name,
			'url' => $url,
			'media_code' => $media_code,
			'call_me' => $call_me,
			'frequency' => $frequency,
			'loyalty' => $loyalty,
			'share' => $share,
			'method' => $method,
			'favorite_way' => $favorite_way,
			'also_enjoys' => $also_enjoys,
			'last_time' => $last_time,
			'ave_no_drinks' => $ave_no_drinks,
			'bar_or_rest' => $bar_or_rest,
			'last_ten' => $last_ten,
			'how_other' => $how_other,
			'lang_id' => $lang_id,
		);

		try{
			$query = $this->createQueryString($fields);
			$consumer = array("nvs"=>$query);
			$validation = $this->client->RegisterByString($consumer);
			$result = $validation->RegisterByStringResult;
		}
		catch(Exception $e){
			$result = 0;
		}

		return $result;
	}

	public function createConsumerWithArray($fields)
	{
		$result = 0;

		try{
			$query = $this->createQueryString($fields);
			$consumer = array("nvs"=>$query);
			$validation = $this->client->RegisterByString($consumer);
			$result = $validation->RegisterByStringResult;
		}
		catch(Exception $e){
			$result = 0;
		}

		return $result;
	}

	public function brandOptOut($strEmail, $iBrandId, $iSourceCode = 0)
	{
		$result = true;
		try{
			$params = array(
				"strEmail" => $strEmail,
				"iBrandId" => $iBrandId,
				"iSourceCode" => $iSourceCode);
			$validation = $this->client->BrandOptOut($params);
			//$result = $validation->BrandOptOutResult;
			//$result === TRUE ? $result = "true" : $result = "false";
		}
		catch(Exception $e){
			$result = false;
		}

		return $result;
	}

	/****************************************************************************************************
	// BF - Private Methods
	****************************************************************************************************/

	private function createQueryString($fields)
	{
		$query = "";

		if($this->is_assoc_array($fields)){
			foreach($fields as $key => $value){
				$query .= $key.'='.$value.'&';
			}
			$query = rtrim($query,'&');
		}

		return $query;
	}

	private function is_assoc_array($array)
	{
    	return ( is_array($array) && ( count($array)==0 || 0 !== count( array_diff_key( $array,array_keys(array_keys($array))) )));
	}
}
